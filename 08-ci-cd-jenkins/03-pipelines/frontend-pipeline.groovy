podTemplate(
    label: 'questcode', 
    name: 'docker',
    namespace: 'devops', 
    containers: [
        containerTemplate(alwaysPullImage: false, args: 'cat', command: '/bin/sh -c', image: 'docker', livenessProbe: containerLivenessProbe(execArgs: '', failureThreshold: 0, initialDelaySeconds: 0, periodSeconds: 0, successThreshold: 0, timeoutSeconds: 0), name: 'docker-container', resourceLimitCpu: '', resourceLimitMemory: '', resourceRequestCpu: '', resourceRequestMemory: '', ttyEnabled: true, workingDir: '/home/jenkins/agent'),
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v3.2.0', name: 'helm-container', ttyEnabled: true)
    ], 
    volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')], 
)
{
    node('questcode') {
        def repos
        stage('Checkout') {
            echo 'Iniciando Clone do Repositório'
            repos = git credentialsId: 'gitlab', url: 'git@gitlab.com:rl-questcode/frontend.git'       
        }
        stage('Package') {
            container('docker-container') {
               echo 'Iniciando empacotamento com Docker'
               withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'DOCKER_HUB_PASSWORD', usernameVariable: 'DOCKER_HUB_USER')]) {
                    sh "docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}"
                    sh "docker build -t rodrigolimasd/qc-brackend-scm:alpha-staging.2 . --build-arg NPM_ENV='staging'"
                    sh "docker push rodrigolimasd/qc-brackend-scm:alpha-staging.2"
               }
            }
        }
        stage('Deploy') {
            container('helm-container') {
                sh 'helm repo add questcode http://helm-chartmuseum:8080'
                sh 'helm repo list'
                sh 'helm upgrade staging-frontend questcode/frontend --set image.tag=alpha-staging.2' 
            }            
        }
    }
}